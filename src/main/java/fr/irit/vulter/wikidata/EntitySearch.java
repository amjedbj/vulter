package fr.irit.vulter.wikidata;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EntitySearch {
	private String normalize(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		str = str.toLowerCase();
		str = str.replaceAll("\\W++", "-");
		return str;
	}

	public JsonNode searchEntities(String search, String language) {
		return this.searchEntities(search, language, 7);
	}

	public Set<String> whatis(String ids, String language) {
		Set<String> types = new HashSet<String>();
		JsonNode node = this.getProperties(ids);

		List<String> properties = new ArrayList<String>();
		properties.add("31");
		properties.add("279");
		properties.add("373");

		for (String property : properties) {
			if (node.get("props").has(property)) {
				String label = null;
				if (node.get("props").get(property).get(0).get(1).asText()
						.equals("item")) {
					String value = node.get("props").get(property).get(0)
							.get(2).asText();
					JsonNode entity = this.getEntities("Q" + value);
					try {
						label = entity.get("entities").get("Q" + value)
								.get("labels").get(language).get("value")
								.asText();
					} catch (Exception e) {

					}
				} else if (node.get("props").get(property).get(0).get(1)
						.asText().equals("string")) {
					label = node.get("props").get(property).get(0).get(2)
							.asText();
				}
				if (label != null)
					types.add(label);
			}

		}
		return types;
	}

	public JsonNode getProperties(String ids) {
		try {
			File cache = new File("cache/wikidata/prop/" + this.normalize(ids));
			String content = null;
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else {
				String qURL = "https://wdq.wmflabs.org/api";

				qURL += "?q=items[" + ids.replaceAll("\\D", "") + "]";
				qURL += "&props=*";
				qURL += "&format=json";

				URL url = new URL(qURL);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				int statusCode = conn.getResponseCode();
				if (statusCode == 200) {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader((conn.getInputStream())));
					StringBuilder strBuilder = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						strBuilder.append(line);
					}
					content = strBuilder.toString();
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			if (content != null) {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode results = mapper.readValue(content, JsonNode.class);
				return results;
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public JsonNode getEntities(String ids) {

		try {
			File cache = new File("cache/wikidata/get/" + this.normalize(ids));
			String content = null;
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else {
				String qURL = "https://www.wikidata.org/w/api.php";

				qURL += "?action=wbgetentities";
				qURL += "&ids=" + ids;
				qURL += "&format=json";

				URL url = new URL(qURL);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				int statusCode = conn.getResponseCode();
				if (statusCode == 200) {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader((conn.getInputStream())));
					StringBuilder strBuilder = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						strBuilder.append(line);
					}
					content = strBuilder.toString();
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			if (content != null) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					JsonNode results = mapper
							.readValue(content, JsonNode.class);
					return results;
				} catch (Exception e) {

				}
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public JsonNode searchEntities(String search, String language, int limit) {

		try {
			File cache = new File("cache/wikidata/search/"
					+ this.normalize(language + "-" + search));
			String content = null;
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else {
				String qURL = "https://www.wikidata.org/w/api.php";
				search = URLEncoder.encode(search,
						StandardCharsets.UTF_8.toString());

				qURL += "?action=wbsearchentities";
				qURL += "&language=" + language;
				qURL += "&search=" + search;
				qURL += "&format=json";

				URL url = new URL(qURL);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				int statusCode = conn.getResponseCode();
				if (statusCode == 200) {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader((conn.getInputStream())));
					StringBuilder strBuilder = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						strBuilder.append(line);
					}
					content = strBuilder.toString();
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			if (content != null) {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode results = mapper.readValue(content, JsonNode.class);
				return results;
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static void main(String[] args) {

		Set<String> types = new HashSet<String>();
		EntitySearch entitySearch = new EntitySearch();
		JsonNode node = entitySearch.searchEntities("hello kitty", "en");
		if (node.has("search")) {
			for (JsonNode result : node.get("search")) {
				System.out.println("\t" + result.get("id") + "\t"
						+ result.get("label"));
				types.addAll(entitySearch.whatis(result.get("id").asText(),
						"en"));
			}
		}

		System.out.println(types);

	}

}
