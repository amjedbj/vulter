/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections15.Transformer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;
import org.xml.sax.SAXException;

import fr.irit.nestor.scoring.Scorer;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiDocFeedback;
import fr.irit.vulter.ll4ir.api.ApiDocumentList;
import fr.irit.vulter.ll4ir.api.ApiFeedback;
import fr.irit.vulter.ll4ir.api.ApiFeedbacks;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ApiQueries;
import fr.irit.vulter.ll4ir.api.ApiQuery;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.scoring.ApiQueryTransformer;
import fr.irit.vulter.scoring.Dictionary;
import fr.irit.vulter.scoring.LLQuery;
import fr.irit.zeutils.CliApplication;
import fr.irit.zeutils.ConfigurationUtils;

public class TestQuery extends CliApplication {

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option scorer = OptionBuilder.withLongOpt("scorer")
					.withDescription("The scoring function").hasArg()
					.isRequired(true).create("f");
			options.addOption(scorer);

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("query")
					.withDescription("The query identifier.").hasArg()
					.isRequired(true).create("q");
			options.addOption(site);

		}
		return this.options;
	}

	public void run(String query, String scorer) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, IOException {
		if (!query.startsWith("R-")) {
			throw new UnsupportedOperationException();
		}
		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		ApiQueries queries = api.query(key);
		Dictionary dictionary = new Dictionary();
		dictionary.load(new File(ConfigurationUtils.getProperty(
				"stats.dictionary", "living-labs.properties")));
		Transformer<ApiQuery, Textual> qtransformer = new ApiQueryTransformer(
				new HungarianAnalyzer(), dictionary);
		Scorer ranker = (Scorer) Class.forName(scorer).newInstance();

		ApiFeedbacks feedback = api.feedback(query, key);
		Set<String> clicked = new TreeSet<String>();
		for (ApiFeedback feed : feedback.getFeedback()) {
			if (feed.qid.equals(query)) {
				for (ApiDocFeedback back : feed.doclist) {
					if (back.isClicked()) {
						if (!clicked.contains(back.getDocid()))
							clicked.add(back.getDocid());
					}

				}

			}

		}

		for (ApiQuery q : queries.getQueries()) {
			if (q.getQid().equals(query)) {
				double outcome=0d;
				LLQuery topic = new LLQuery(q, qtransformer);
				ApiDocumentList docslist = api.doclist(q.getQid(), key);
				ResultSet<ApiProduct> resultset = docslist.toProductResultSet(
						api, key);
				ranker.process(topic, resultset);
				System.out.println("Query " + q.getQid() + ": \""
						+ topic.getText());
				System.out.println(resultset.size() + " results found");
				System.out.println("\nPos\tRel\tAvail\tScore\tID\tTitle");
				int k = 1;
				for (ResultItem<ApiProduct> resultItem : resultset) {
					int rel=clicked.contains(resultItem.getDocument().getDocid()) ? 1 : 0;
					int avil=resultItem.getDocument().getContent().getAvailable();
					if(rel==1 && avil==1) outcome+=1.d/k;
					System.out.println(k + "\t"
							+rel+ "\t"
									+avil+ "\t"
							+ String.format("%.2f", resultItem.getScore())
							+ "\t" + resultItem.getDocument().getDocid() + "\t"
							+ resultItem.getDocument().getTitle() + "\t");
					k++;
				}
				System.out.println("RevRank : "+ outcome);
				break;
			}

		}
	}

	public static void main(String[] args) throws IOException,
			ParserConfigurationException, SAXException, InterruptedException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		TestQuery app = new TestQuery();
		try {
			CommandLine line = app.parseLine(args);
			String scorer = line.getOptionValue("scorer");
			String query = line.getOptionValue("query");
			app.run(query, scorer);
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
