/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.io.IOUtils;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;
import org.xml.sax.SAXException;

import fr.irit.nestor.scoring.Scorer;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiDocumentList;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ApiQueries;
import fr.irit.vulter.ll4ir.api.ApiQuery;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.scoring.ApiQueryTransformer;
import fr.irit.vulter.scoring.Dictionary;
import fr.irit.vulter.scoring.LLQuery;
import fr.irit.zeutils.CliApplication;
import fr.irit.zeutils.ConfigurationUtils;

public class ViewRun extends CliApplication {

	public static final String DEFAULT_PHOTO = "http://www.regiojatek.hu/images/regio_logo.png";

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option scorer = OptionBuilder.withLongOpt("scorer")
					.withDescription("The scoring function").hasArg()
					.isRequired(true).create("f");
			options.addOption(scorer);

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("site")
					.withDescription("The site's short name.").hasArg()
					.isRequired(true).create("s");
			options.addOption(site);

			@SuppressWarnings("static-access")
			Option output = OptionBuilder.withLongOpt("output")
					.withDescription("The output ndjson file of run").hasArg()
					.isRequired(true).create("o");
			options.addOption(output);

		}
		return this.options;
	}

	public void run(String site, String scorer, File file)
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, IOException, java.text.ParseException {
		if (!site.equals("R")) {
			throw new UnsupportedOperationException();
		}
		if (file.exists())
			file.delete();
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				file, true)));

		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		ApiQueries queries = api.query(key);
		Dictionary dictionary = new Dictionary();
		dictionary.load(new File(ConfigurationUtils.getProperty(
				"stats.dictionary", "living-labs.properties")));

		Transformer<ApiQuery, Textual> qtransformer = new ApiQueryTransformer(
				new HungarianAnalyzer(), dictionary);

		Scorer ranker = (Scorer) Class.forName(scorer).newInstance();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

		String template = IOUtils.toString(new FileInputStream("resources/template/run.template.html")) ;// IOUtils.toString(this.getClass().getClassLoader()
				//.getResourceAsStream("template/run.template.html"));
		StringBuilder content = new StringBuilder();
		for (ApiQuery q : queries.getQueries()) {
			if (q.getQid().startsWith(site + "-")) {
				LLQuery query = new LLQuery(q, qtransformer);
				System.out.println(query.getDocument().getQid() + "\t"
						+ query.getDocument().getQstr() + "\t"
						+ query.getDocument().getType() + "\t"
						+ dateFormat.format(query.getDocument().getDate()));
				ApiDocumentList docslist = api.doclist(q.getQid(), key);

				ResultSet<ApiProduct> resultset = docslist.toProductResultSet(
						api, key);
				ranker.process(query, resultset);

				String qtemplate = IOUtils.toString(new FileInputStream("resources/template/query.template.html")) ; //IOUtils.toString(this.getClass()
						//.getClassLoader()
						//.getResourceAsStream("template/query.template.html"));

				qtemplate = qtemplate.replace("{id}", query.getDocument()
						.getQid());
				qtemplate = qtemplate.replace("{str}", query.getDocument()
						.getQstr());
				qtemplate = qtemplate.replace("{label}", (query.getDocument()
						.getType().equals("train") ? "default" : "primary"));
				qtemplate = qtemplate.replace("{type}", query.getDocument()
						.getType());

				StringBuilder products = new StringBuilder();
				for (ResultItem<ApiProduct> resultItem : resultset) {

					String ptemplate =IOUtils.toString(new FileInputStream("resources/template/product.template.html")) ; /*IOUtils.toString(this
							.getClass()
							.getClassLoader()
							.getResourceAsStream(
									"template/product.template.html"));*/

					String photo = (resultItem.getDocument().getContent()
							.getPhotos().iterator().hasNext()) ? resultItem
							.getDocument().getContent().getPhotos().iterator()
							.next() : ViewRun.DEFAULT_PHOTO;
					ptemplate = ptemplate.replace("{photo}", photo);
					ptemplate = ptemplate.replace("{brand}", resultItem
							.getDocument().getContent().getBrand());
					ptemplate = ptemplate.replace("{name}", resultItem
							.getDocument().getContent().getProduct_name());
					ptemplate = ptemplate.replace("{category}", resultItem
							.getDocument().getContent().getCategory());
					if (resultItem.getDocument().getContent().getCharacters() != null)
						ptemplate = ptemplate.replace("{characters}",
								resultItem.getDocument().getContent()
										.getCharacters().toString());
					ptemplate = ptemplate.replace(
							"{price}",
							Double.toString(resultItem.getDocument()
									.getContent().getPrice()));
					ptemplate = ptemplate.replace("{short-description}",
							resultItem.getDocument().getContent()
									.getShort_description());
					products.append(ptemplate);
				}
				qtemplate = qtemplate
						.replace("{products}", products.toString());
				content.append(qtemplate);
			}

		}
		String styles = IOUtils.toString(new FileInputStream("resources/template/styles.css")) ; // IOUtils.toString(this.getClass().getClassLoader()
				//.getResourceAsStream("template/styles.css"));
		template = template.replace("{style}", styles.toString());
		template = template.replace("{content}", content.toString());
		out.println(template);
		out.close();
	}

	public static void main(String[] args) throws IOException,
			ParserConfigurationException, SAXException, InterruptedException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException, java.text.ParseException {
		ViewRun app = new ViewRun();
		try {
			CommandLine line = app.parseLine(args);
			String scorer = line.getOptionValue("scorer");
			String site = line.getOptionValue("site");
			File file = new File(line.getOptionValue("output"));
			app.run(site, scorer, file);
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
