/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections15.Transformer;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.nestor.scoring.Scorer;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiDocumentList;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ApiQueries;
import fr.irit.vulter.ll4ir.api.ApiQuery;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.scoring.ApiQueryTransformer;
import fr.irit.vulter.scoring.LLQuery;
import fr.irit.zeutils.CliApplication;

public class Outcome extends CliApplication {

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

		}
		return this.options;
	}

	public void run() throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {

		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		ApiQueries queries = api.query(key);
		String round = "LL4IR Round #3";
		for (ApiQuery q : queries.getQueries()) {
			if (q.getQid().startsWith("R-")) {
				JsonNode node = api.outcome(q.getQid(), key);
				JsonNode outcomeNode = null;
				if (node.get("outcomes").size() > 0) {
					for (JsonNode oNode : node.get("outcomes")) {
						if (oNode.get("type").asText().equals("train")) {
							outcomeNode = oNode;
							break;

						} else {
							if (oNode.get("test_period").get("name").asText()
									.equals(round)) {
								outcomeNode = oNode;
								break;
							}
						}

					}
				}
				double outcome = outcomeNode!=null? outcomeNode.get("outcome").asDouble() : 0d;
				int wins =outcomeNode!=null? outcomeNode.get("wins").asInt() :0;
				int losses = outcomeNode!=null?outcomeNode.get("losses").asInt():0;
				int ties = outcomeNode!=null?outcomeNode.get("ties").asInt():0;
				int impressions = outcomeNode!=null?outcomeNode.get("impressions").asInt():0;
				String line = q.getQid() + "\t" + q.getType() + "\t"
						+ q.getQstr() + "\t" + outcome + "\t" + wins + "\t"
						+ losses + "\t" + ties + "\t" + impressions;
				System.out.println(line);
			}

		}
	}

	public static void main(String[] args) throws IOException,
			ParserConfigurationException, SAXException, InterruptedException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Outcome app = new Outcome();
		try {
			CommandLine line = app.parseLine(args);
			app.run();
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
