/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.ll4ir.api.Run;
import fr.irit.zeutils.CliApplication;

public class SubmitRun extends CliApplication {

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option input = OptionBuilder.withLongOpt("input")
					.withDescription("The input ndjson file of run").hasArg()
					.isRequired(true).create("i");
			options.addOption(input);

		}
		return this.options;
	}

	public void run(File file) throws IOException {
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		ObjectMapper mapper= new ObjectMapper();
		ParticipantAPI api= new ParticipantAPI();
		String key= api.getKey();
		try {
			while (it.hasNext()) {
				String line = it.nextLine();
				Run run= mapper.readValue(line, Run.class);
				String qid=run.getQid();
				System.out.println(qid);
				api.putRun(run, qid, key);
			}
		} finally {
			it.close();
		}

	}

	public static void main(String[] args) throws IOException {
		SubmitRun app = new SubmitRun();
		try {
			CommandLine line = app.parseLine(args);
			File file = new File(line.getOptionValue("input"));
			app.run(file);
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
