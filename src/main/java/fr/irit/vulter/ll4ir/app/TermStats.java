/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.scoring.ProductTransformer;
import fr.irit.zeutils.CliApplication;

public class TermStats extends CliApplication {

	protected Map<String, Term> dictionary = new TreeMap<String, Term>();

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("site")
					.withDescription("The site's short name.").hasArg()
					.isRequired(true).create("s");
			options.addOption(site);

			@SuppressWarnings("static-access")
			Option output = OptionBuilder.withLongOpt("output")
					.withDescription("The output file.").hasArg()
					.isRequired(true).create("o");
			options.addOption(output);

		}
		return this.options;
	}

	public void incrementDF(Term term) {
		if (!this.dictionary.containsKey(term.getText())) {
			this.dictionary.put(term.getText(), term);
		} else {
			Term tmp = this.dictionary.get(term.getText());
			long df = tmp.getDf();
			tmp.setDf(df + 1);
		}

	}

	public void incrementCF(Term term) {
		if (!this.dictionary.containsKey(term.getText())) {
			this.dictionary.put(term.getText(), term);
		} else {
			Term tmp = this.dictionary.get(term.getText());
			long cf = tmp.getCf();
			tmp.setCf(cf + 1);
		}

	}

	public void run(String site, File file) throws IOException {

		if (file.exists())
			file.delete();
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				file, true)));

		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		JsonNode docs = api.docs(key);
		ProductTransformer transformer = new ProductTransformer(
				new HungarianAnalyzer());
		System.out
				.println("Analyzing documents! This make takes several minutes.");
		for (JsonNode doc : docs.get("docs")) {
			String site_id = (doc.has("site_id")) ? doc.get("site_id").asText()
					: null;
			if (site_id != null) {
				if (site_id.equals("R")) {
					String docid = (doc.has("docid")) ? doc.get("docid")
							.asText() : null;
					try {
						ApiProduct product = api.product(docid, api.getKey());

						Textual textual = transformer.transform(product);
						for (Term term : textual.termSet()) {
							this.incrementDF(term);
						}
						for (Term term : textual.termList()) {
							this.incrementCF(term);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		for (String str : this.dictionary.keySet()) {
			Term term = this.dictionary.get(str);
			String line = term + "\t" + term.getDf() + "\t" + term.getCf();
			out.println(line);

		}
		out.close();
		System.out.println(this.dictionary.size() + " terms found");
	}

	public static void main(String[] args) throws IOException {
		TermStats app = new TermStats();
		try {
			CommandLine line = app.parseLine(args);
			String site = line.getOptionValue("site");
			File file = new File(line.getOptionValue("output"));
			app.run(site, file);
		} catch (ParseException e) {
			app.printHelp();
		}
	}
}
