/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections15.Transformer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiQueries;
import fr.irit.vulter.ll4ir.api.ApiQuery;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.scoring.ApiQueryTransformer;
import fr.irit.vulter.scoring.Dictionary;
import fr.irit.vulter.scoring.LLQuery;
import fr.irit.zeutils.CliApplication;
import fr.irit.zeutils.ConfigurationUtils;

public class CheckRun extends CliApplication {

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("site")
					.withDescription("The site's short name.").hasArg()
					.isRequired(true).create("s");
			options.addOption(site);

		}
		return this.options;
	}

	public void run(String site) throws IOException {
		if (!site.equals("R")) {
			throw new UnsupportedOperationException();
		}

		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		ApiQueries queries = api.query(key);
		Dictionary dictionary = new Dictionary();
		dictionary.load(new File(ConfigurationUtils.getProperty(
				"stats.dictionary", "living-labs.properties")));

		Transformer<ApiQuery, Textual> qtransformer = new ApiQueryTransformer(
				new HungarianAnalyzer(), dictionary);
		Map<String, Integer> runs = new Hashtable<String, Integer>();
		for (ApiQuery q : queries.getQueries()) {
			if (q.getQid().startsWith(site + "-")) {
				LLQuery query = new LLQuery(q, qtransformer);

				JsonNode run = api.getRun(query.getDocument().getQid(), key);

				String runid = run.get("runid").asText();
				if (!runs.containsKey(runid))
					runs.put(runid, 0);
				runs.put(runid, runs.get(runid) + 1);
				
				System.out.println(query.getDocument().getQid() + "\t"
						+ query.getDocument().getQstr() + "\t"
						+ query.getDocument().getType() + "\t" + runid+"\t"+run.get("creation_time").asText());

			}
		}

		int k = 0;
		for (String runid : runs.keySet()) {
			
			System.out.println("Total " + runid + ": " + runs.get(runid));
			k += runs.get(runid);
		}
		System.out.println("Total all runs: " + k);

	}

	public static void main(String[] args) throws IOException {
		CheckRun app = new CheckRun();
		try {
			CommandLine line = app.parseLine(args);
			String site = line.getOptionValue("site");
			app.run(site);
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
