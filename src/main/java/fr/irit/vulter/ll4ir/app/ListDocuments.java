/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.zeutils.CliApplication;

public class ListDocuments extends CliApplication {

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("site")
					.withDescription("The site's short name.").hasArg()
					.isRequired(true).create("s");
			options.addOption(site);

		}
		return this.options;
	}

	public void run(String site) throws IOException {
		int k = 0;
		ParticipantAPI api = new ParticipantAPI();
		System.out.println("Num\tDocs\tTitle");
		String key = api.getKey();
		JsonNode docs = api.docs(key);
		for (JsonNode doc : docs.get("docs")) {
			String site_id = (doc.has("site_id")) ? doc.get("site_id").asText()
					: null;
			if (site_id != null) {
				if (site_id.equals(site)) {
					String docid = (doc.has("docid")) ? doc.get("docid")
							.asText() : null;
					try {
						ApiProduct product = api.product(docid, api.getKey());
						System.out.println((k + 1) + "\t" + docid + "\t"
								+ product.getTitle());
					} catch (Exception e) {
						e.printStackTrace();
					}
					k++;
				}
			}
		}
	}

	public static void main(String[] args) throws IOException {
		ListDocuments app = new ListDocuments();
		try {
			CommandLine line = app.parseLine(args);
			String site = line.getOptionValue("site");
			app.run(site);
		} catch (ParseException e) {
			app.printHelp();
		}
	}
}
