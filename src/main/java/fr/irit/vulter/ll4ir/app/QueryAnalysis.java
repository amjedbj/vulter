/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import info.debatty.java.stringsimilarity.Jaccard;
import info.debatty.java.stringsimilarity.interfaces.StringSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections15.Transformer;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ApiQueries;
import fr.irit.vulter.ll4ir.api.ApiQuery;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.scoring.ApiQueryTransformer;
import fr.irit.vulter.scoring.Dictionary;
import fr.irit.vulter.scoring.LLQuery;
import fr.irit.vulter.wikidata.EntitySearch;
import fr.irit.zeutils.CliApplication;
import fr.irit.zeutils.ConfigurationUtils;

public class QueryAnalysis extends CliApplication {

	class EntityComparator implements Comparator<JsonNode> {

		private String text;
		private StringSimilarity similarity = new Jaccard();

		public EntityComparator(String text) {
			this.text = text;
		}

		@Override
		public int compare(JsonNode o1, JsonNode o2) {
			double sim1 = 0d;
			if (o1.has("match")) {
				if (o1.get("match").has("text")) {
					sim1 = this.similarity.similarity(this.text.toLowerCase(),
							o1.get("match").get("text").asText().toLowerCase());
				}
			}

			double sim2 = 0d;
			if (o2.has("match")) {
				if (o2.get("match").has("text")) {
					sim2 = this.similarity.similarity(this.text.toLowerCase(),
							o2.get("match").get("text").asText().toLowerCase());
				}
			}
			double diff = (sim2 - sim1) * 100;
			return (int) diff;

		}
	}

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("site")
					.withDescription("The site's short name.").hasArg()
					.isRequired(true).create("s");
			options.addOption(site);

		}
		return this.options;
	}

	public void run(String site) throws IOException {
		if (!site.equals("R")) {
			throw new UnsupportedOperationException();
		}

		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		ApiQueries queries = api.query(key);
		Dictionary dictionary = new Dictionary();
		dictionary.load(new File(ConfigurationUtils.getProperty(
				"stats.dictionary", "living-labs.properties")));

		Transformer<ApiQuery, Textual> qtransformer = new ApiQueryTransformer(
				new HungarianAnalyzer(), dictionary);

		EntitySearch entitySearch = new EntitySearch();
		int entitylimit = 7;

		ParticipantAPI.allowApiCall =false;
		for (ApiQuery q : queries.getQueries()) {
			if (q.getQid().startsWith(site + "-")) {
				LLQuery query = new LLQuery(q, qtransformer);

				// System.out.println(query.getDocument().getQid() + "\t"
				// + query.getDocument().getQstr());
				String lang = "en";

				List<JsonNode> vlist = new ArrayList<>();
				Set<JsonNode> klist = new HashSet<>();

				JsonNode node = entitySearch.searchEntities(query.getDocument()
						.getQstr(), lang, entitylimit);
				if (node.has("search")) {
					for (JsonNode result : node.get("search")) {
						if (!klist.contains(result.get("id"))) {
							klist.add(result.get("id"));
							vlist.add(result);
						}
					}
				}

				lang = "hu";
				node = entitySearch.searchEntities(query.getDocument()
						.getQstr(), lang, entitylimit);
				if (node.has("search")) {
					for (JsonNode result : node.get("search")) {
						if (!klist.contains(result.get("id"))) {
							klist.add(result.get("id"));
							vlist.add(result);
						}
					}
				}

				Collections.sort(vlist, new EntityComparator(query
						.getDocument().getQstr()));

				StringSimilarity similarity = new Jaccard();

				Set<String> types = new HashSet<String>();

				for (JsonNode result : vlist) {
					double sim = similarity.similarity(query.getDocument()
							.getQstr().toLowerCase(),
							result.get("match").get("text").asText()
									.toLowerCase());
					// System.out.println("\t" + sim + "\t" + result.get("id")
					// + "\t" + result.get("label"));
					types.addAll(entitySearch.whatis(result.get("id").asText(),
							"en"));

				}

				JsonNode docs = api.docs(key);
				boolean isCharacter = false;
				boolean isBrand = false;
				boolean isCategory = false;

				for (JsonNode doc : docs.get("docs")) {
					String docid = (doc.has("docid")) ? doc.get("docid")
							.asText() : null;
					try {
						ApiProduct product = api.product(docid, api.getKey());

						if (product.getContent().getCharacters() != null) {
							for (String Character : product.getContent()
									.getCharacters()) {
								if (query.getDocument().getQstr().toLowerCase()
										.equals(Character.toLowerCase())) {
									isCharacter = true;
									break;
								}
							}
						}

						if (product.getContent().getBrand() != null) {
							if (query
									.getDocument()
									.getQstr()
									.toLowerCase()
									.equals(product.getContent().getBrand()
											.toLowerCase())) {
								isBrand = true;
							}
						}

						if (product.getContent().getCategory() != null) {
							if (query
									.getDocument()
									.getQstr()
									.toLowerCase()
									.equals(product.getContent().getCategory()
											.toLowerCase())) {
								isCategory = true;
							}
						}

						if (product.getContent().getMain_category() != null) {
							if (query
									.getDocument()
									.getQstr()
									.toLowerCase()
									.equals(product.getContent()
											.getMain_category().toLowerCase())) {
								isCategory = true;
							}
						}
					} catch (Exception e) {
					}
				}
				System.out.println(query.getDocument().getQid() + "\t"
						+ query.getDocument().getType() + "\t" + isCharacter
						+ "\t" + isBrand + "\t" + isCategory + "\t"
						+ query.getDocument().getQstr() + "\t" + types);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		QueryAnalysis app = new QueryAnalysis();
		try {
			CommandLine line = app.parseLine(args);
			String site = line.getOptionValue("site");
			app.run(site);
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
