/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.collections15.Transformer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;
import org.xml.sax.SAXException;

import fr.irit.nestor.scoring.Scorer;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiDocumentList;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.ll4ir.api.ApiQueries;
import fr.irit.vulter.ll4ir.api.ApiQuery;
import fr.irit.vulter.ll4ir.api.ParticipantAPI;
import fr.irit.vulter.ll4ir.api.Run;
import fr.irit.vulter.scoring.ApiQueryTransformer;
import fr.irit.vulter.scoring.Dictionary;
import fr.irit.vulter.scoring.LLQuery;
import fr.irit.zeutils.CliApplication;
import fr.irit.zeutils.ConfigurationUtils;

public class MakeRun extends CliApplication {

	@Override
	protected Options getOptions() {
		if (this.options == null) {
			this.options = new Options();

			@SuppressWarnings("static-access")
			Option scorer = OptionBuilder.withLongOpt("scorer")
					.withDescription("The scoring function").hasArg()
					.isRequired(true).create("f");
			options.addOption(scorer);

			@SuppressWarnings("static-access")
			Option site = OptionBuilder.withLongOpt("site")
					.withDescription("The site's short name.").hasArg()
					.isRequired(true).create("s");
			options.addOption(site);

			@SuppressWarnings("static-access")
			Option output = OptionBuilder.withLongOpt("output")
					.withDescription("The output ndjson file of run").hasArg()
					.isRequired(true).create("o");
			options.addOption(output);

			@SuppressWarnings("static-access")
			Option runid = OptionBuilder.withLongOpt("runid")
					.withDescription("The run identifier").hasArg()
					.isRequired(false).create("r");
			options.addOption(runid);

		}
		return this.options;
	}

	protected void print(LLQuery topic, ResultSet<ApiProduct> resultset) {
		System.out.println("Query " + topic.getDocument().getQid() + ": \""
				+ topic.getText());
		System.out.println(resultset.size() + " results found");
		System.out.println("\nPos\tScore\tID\tTitle");
		int k = 1;
		for (ResultItem<ApiProduct> resultItem : resultset) {
			System.out.println(k + "\t"
					+ String.format("%.5f", resultItem.getScore()) + "\t"
					+ resultItem.getDocument().getDocid() + "\t"
					+ resultItem.getDocument().getTitle() + "\t");
			k++;
		}

	}

	public void run(String site, String scorer, String runid, File file)
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, IOException, java.text.ParseException {
		if (!site.equals("R")) {
			throw new UnsupportedOperationException();
		}
		if (file.exists())
			file.delete();
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				file, true)));

		ParticipantAPI api = new ParticipantAPI();
		String key = api.getKey();
		ApiQueries queries = api.query(key);
		Dictionary dictionary = new Dictionary();
		dictionary.load(new File(ConfigurationUtils.getProperty(
				"stats.dictionary", "living-labs.properties")));

		Transformer<ApiQuery, Textual> qtransformer = new ApiQueryTransformer(
				new HungarianAnalyzer(), dictionary);

		Scorer ranker = (Scorer) Class.forName(scorer).newInstance();

		for (ApiQuery q : queries.getQueries()) {
			if (q.getQid().startsWith(site + "-")) {
				LLQuery query = new LLQuery(q, qtransformer);
				System.out.println(query.getDocument().getQid() + "\t"
						+ query.getDocument().getQstr() + "\t"
						+ query.getDocument().getType());

				ApiDocumentList docslist = api.doclist(q.getQid(), key);

				ResultSet<ApiProduct> resultset = docslist.toProductResultSet(
						api, key);
				ranker.process(query, resultset);
				Run run = new Run(query.getDocument().getQid(), runid,
						resultset);
				out.println(run.toJson());
				out.flush();
			}

		}
		out.close();
	}

	public static void main(String[] args) throws IOException,
			ParserConfigurationException, SAXException, InterruptedException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException, java.text.ParseException {
		MakeRun app = new MakeRun();
		try {
			CommandLine line = app.parseLine(args);
			String scorer = line.getOptionValue("scorer");
			String site = line.getOptionValue("site");
			String runid = (line.hasOption("runid")) ? line
					.getOptionValue("runid") : "vulter";
			File file = new File(line.getOptionValue("output"));
			app.run(site, scorer, runid, file);
		} catch (ParseException e) {
			app.printHelp();
		}

	}
}
