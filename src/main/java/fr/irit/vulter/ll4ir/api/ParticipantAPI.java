/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ParticipantAPI {

	private static final String PROPERTIES_FILE = "living-labs.properties";
	
	public static boolean allowApiCall=true;

	public ParticipantAPI() {
	}

	private URI getBaseURI() {
		try {
			Properties properties = new Properties();

			File file = new File(ParticipantAPI.PROPERTIES_FILE);
			InputStream in = new FileInputStream(file);
			properties.load(in);
			in.close();

			String host = properties.getProperty("api.host");
			String port = properties.getProperty("api.port");
			return new URI("http://" + host + ":" + port + "/");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Properties getProperties() {

		try {
			Properties properties = new Properties();
			File file = new File("living-labs.properties");
			InputStream in = new FileInputStream(file);
			properties.load(in);
			in.close();
			return properties;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getQueryMethodPath() {
		return this.getProperties().getProperty("api.method.doclist.query");
	}

	private String getDocListMethodPath() {
		return this.getProperties().getProperty("api.method.doclist.doclist");
	}

	private String getDocMethodPath() {
		return this.getProperties().getProperty("api.method.doclist.doc");
	}

	private String getDocsMethodPath() {
		return this.getProperties().getProperty("api.method.docs");
	}

	private String getRunMethodPath() {
		return this.getProperties().getProperty("api.method.doclist.run");
	}

	private String getOutcomeMethodPath() {
		return this.getProperties().getProperty("api.method.outcome");
	}

	private String excute(HttpUriRequest request) {
		try {

			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpResponse response = httpclient.execute(request);

			// Build HTTP String response
			String line;
			StringBuilder strBuilder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			while ((line = reader.readLine()) != null) {
				strBuilder.append(line);
			}
			return strBuilder.toString();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unused")
	private String put(HttpUriRequest request) {
		try {
			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpResponse response = httpclient.execute(request);

			// Build HTTP String response
			String line;
			StringBuilder strBuilder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			while ((line = reader.readLine()) != null) {
				strBuilder.append(line);
			}
			return strBuilder.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getKey() {
		try {
			Properties properties = new Properties();

			File file = new File(ParticipantAPI.PROPERTIES_FILE);
			InputStream in = new FileInputStream(file);
			properties.load(in);
			in.close();

			return properties.getProperty("api.participant.key");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ApiQueries query(String key) {
		String path = this.getQueryMethodPath();
		path = path.replace("(key)", key);
		try {
			File cache = new File("cache/ll4ir/queries.json");
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			ApiQueries queries = mapper.readValue(content, ApiQueries.class);
			return queries;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public ApiProduct doc(String docid, String key) {
		throw new UnsupportedOperationException();
	}

	public ApiProduct product(String docid, String key) {
		String path = this.getDocMethodPath();
		path = path.replace("(key)", key);
		path = path.replace("(docid)", docid);
		try {
			File cache = new File("cache/ll4ir/docs/" + docid);
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			ApiProduct doc = mapper.readValue(content, ApiProduct.class);
			return doc;
		} catch (URISyntaxException e) {
			//e.printStackTrace();
		} catch (JsonParseException e) {
			//e.printStackTrace();
		} catch (JsonMappingException e) {
			//e.printStackTrace();
		} catch (IOException e) {
		}
		return null;
	}

	public ApiDocumentList doclist(String qid, String key) {
		String path = this.getDocListMethodPath();
		path = path.replace("(key)", key);
		path = path.replace("(qid)", qid);
		try {

			File cache = new File("cache/ll4ir/doclist/" + qid);
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			ApiDocumentList doclist = mapper.readValue(content,
					ApiDocumentList.class);

			return doclist;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}

		return null;
	}

	public JsonNode docs(String key) {
		String path = this.getDocsMethodPath();
		path = path.replace("(key)", key);
		try {

			File cache = new File("cache/ll4ir/docs.json");
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			JsonNode docs = mapper.readValue(content, JsonNode.class);

			return docs;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}

		return null;
	}

	public ApiDocumentList run(String qid, String key) {
		return null;
	}

	public JsonNode getRun(String qid, String key) {
		String path = this.getRunMethodPath();
		path = path.replace("(key)", key);
		path = path.replace("(qid)", qid);
		try {
			URI uri = new URI(this.getBaseURI() + path);
			HttpGet request = new HttpGet(uri);
			request.setHeader("Content-type", "application/json");
			String content = this.excute(request);
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(content, JsonNode.class);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}

		return null;
	}

	public void putRun(Run run, String qid, String key) {
		String path = this.getRunMethodPath();
		path = path.replace("(key)", key);
		path = path.replace("(qid)", qid);
		try {
			URI uri = new URI(this.getBaseURI() + path);
			HttpPut request = new HttpPut(uri);
			StringEntity jsonRun = new StringEntity(run.toJson());
			request.setEntity(jsonRun);
			request.setHeader("Content-type", "application/json");
			this.excute(request);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}

	}

	private String getFeedbackAllMethodPath() {
		return this.getProperties().getProperty("api.method.feedback.all");
	}

	public JsonNode outcome(String qid, String key) {
		String path = this.getOutcomeMethodPath();
		path = path.replace("(key)", key);
		path = path.replace("(qid)", qid);
		try {

			File cache = new File("cache/ll4ir/outcome/"+qid);
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			JsonNode response = mapper.readValue(content,
					JsonNode.class);
			return response;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
		return null;
	}
	public ApiFeedbacks allFeedback(String key) {
		String path = this.getFeedbackAllMethodPath();
		path = path.replace("(key)", key);
		try {
	
			File cache = new File("cache/ll4ir/feedback/all");
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			ApiFeedbacks response = mapper.readValue(content,
					ApiFeedbacks.class);
			return response;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
		return null;
	}

	public ApiFeedbacks feedback(String qid, String key) {
		String path = "api/participant/feedback/(key)/(qid)";
		path = path.replace("(key)", key);
		path = path.replace("(qid)", qid);
		try {
	
			File cache = new File("cache/ll4ir/feedback/"+qid);
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else if(ParticipantAPI.allowApiCall) {
				URI uri = new URI(this.getBaseURI() + path);
				HttpGet request = new HttpGet(uri);
				content = this.excute(request);
				if (!content.isEmpty()) {
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			ApiFeedbacks response = mapper.readValue(content,
					ApiFeedbacks.class);
			return response;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
		return null;
	}

}
