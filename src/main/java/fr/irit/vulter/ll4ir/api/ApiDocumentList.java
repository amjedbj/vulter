/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections15.Transformer;

import fr.irit.nestor.searching.ResultItemFactory;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.searching.ResultSetFactory;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.scoring.ProductTransformer;

public class ApiDocumentList {
	protected List<ApiDocument> doclist = new ArrayList<ApiDocument>();
	protected String qid;

	public ApiDocumentList() {

	}

	public List<ApiDocument> getDoclist() {
		return doclist;
	}

	public void setDoclist(List<ApiDocument> doclist) {
		this.doclist = doclist;
	}

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public ResultSet<ApiProduct> toProductResultSet(ParticipantAPI api,
			String key) {

		Transformer<ApiProduct, Textual> ptransformer = new ProductTransformer();

		ResultItemFactory<ApiProduct> itemfactory = new ResultItemFactory<ApiProduct>(
				ptransformer);
		ResultSetFactory<ApiProduct> setfactory = new ResultSetFactory<ApiProduct>(
				itemfactory);

		List<ApiProduct> products = new ArrayList<ApiProduct>();
		for (ApiDocument doc : this.getDoclist()) {
			ApiProduct product = api.product(doc.getDocid(), key);
			products.add(product);
		}
		return setfactory.getResultSet(products);
	}

}
