/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;

public class Run {
	private String qid;
	private String runid;
	private String creation_time;
	private List<ApiDocId> doclist = new ArrayList<ApiDocId>();

	public Run() {
		this.initCreationTime();
	}

	public Run(String qid, String runid) {
		this.qid = qid;
		this.runid = runid;
		this.initCreationTime();
	}

	public Run(String qid, String runid, ResultSet<ApiProduct> resultSet) {
		this.qid = qid;
		this.runid = runid;
		this.initCreationTime();
		for (ResultItem<ApiProduct> resultitem : resultSet) {
			doclist.add(new ApiDocId(resultitem.getDocument().getDocid()));
		}
	}

	private void initCreationTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(
				"EEE, dd MMM yyyy HH:mm:ss ZZZZZ", Locale.US);
		this.creation_time = sdf.format(new Date());
	}

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public String getRunid() {
		return runid;
	}

	public void setRunid(String runid) {
		this.runid = runid;
	}

	public String getCreation_time() {
		return creation_time;
	}

	public void setCreation_time(String creation_time) {
		this.creation_time = creation_time;
	}

	public List<ApiDocId> getDoclist() {
		return doclist;
	}

	public void setDoclist(List<ApiDocId> doclist) {
		this.doclist = doclist;
	}

	public void addDocument(ApiDocId document) {

		this.getDoclist().add(document);
	}

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "{}";
	}
}
