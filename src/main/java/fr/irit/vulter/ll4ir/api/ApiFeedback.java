/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.ll4ir.api;

import java.util.ArrayList;

public class ApiFeedback {
	public ArrayList<ApiDocFeedback> doclist = new ArrayList<ApiDocFeedback>();
	public String modified_time;
	public String qid;
	public String runid;
	public String type;

	public ApiFeedback() {

	}

	public String getModified_time() {
		return modified_time;
	}

	public void setModified_time(String modified_time) {
		this.modified_time = modified_time;
	}

	public ArrayList<ApiDocFeedback> getDoclist() {
		return doclist;
	}

	public void setDoclist(ArrayList<ApiDocFeedback> doclist) {
		this.doclist = doclist;
	}

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public String getRunid() {
		return runid;
	}

	public void setRunid(String runid) {
		this.runid = runid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
