/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.facebook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.Normalizer;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GraphAPI {
	public static final String PROPERTIES="facebook.properties";
	public static final String URI = "https://graph.facebook.com/v2.3/";
	

	private String normalize(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		str = str.toLowerCase();
		str = str.replaceAll("\\W++", "-");
		return str;
	}

	private Properties getProperties() {

		try {
			Properties properties = new Properties();
			File file = new File(GraphAPI.PROPERTIES);
			InputStream in = new FileInputStream(file);
			properties.load(in);
			return properties;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public FbURL url(URL url){
		try {
			File cache = new File("cache/facebook/"+this.normalize(url.toString()));
			String content = "";
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else {
				
				URIBuilder builder = new URIBuilder(GraphAPI.URI);
				builder.addParameter( "id",url.toString());
				builder.addParameter( "fields","og_object{id,description,title,type,updated_time,url,engagement},share");
				builder.addParameter( "access_token",this.getProperties().getProperty("facebook.accesstoken"));
				HttpGet request = new HttpGet(builder.build());
				HttpClient httpclient = HttpClientBuilder.create().build(); 
				HttpResponse response = httpclient.execute(request);
				StringBuilder strBuilder = new StringBuilder();
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				String line;
				while ((line = reader.readLine()) != null) {
					strBuilder.append(line);
				}
				content= strBuilder.toString();
				FileUtils.writeStringToFile(cache, content, "UTF-8");
			}
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(content, FbURL.class);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}
	public int interactions(URL url){
		FbURL fburl=this.url(url);
		int sum=0;
		sum+=Integer.parseInt(fburl.getShare().getShare_count());
		sum+=Integer.parseInt(fburl.getShare().getComment_count());
		sum+=Integer.parseInt(fburl.getOg_object().engagement.get("count"));
		return sum;
	}
	public static void main(String[] args) throws MalformedURLException {
		GraphAPI link=new GraphAPI();
		URL url=new URL("http://www.google.com");
		System.out.println("interactions "+link.interactions(url));
	}

}
