/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.facebook;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FbURL {
	@JsonIgnoreProperties(ignoreUnknown = true)
	public class OGObject {
		protected String id;
		protected String description;
		protected String title;
		protected String type;
		protected String updated_time;
		protected String url;
		public Map<String,String> engagement;

		public OGObject(){
			
		}
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getUpdated_time() {
			return updated_time;
		}

		public void setUpdated_time(String updated_time) {
			this.updated_time = updated_time;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class Share {
		protected String comment_count;
		protected String share_count;

		public Share(){
			
		}
		public String getComment_count() {
			return comment_count;
		}

		public void setComment_count(String comment_count) {
			this.comment_count = comment_count;
		}

		public String getShare_count() {
			return share_count;
		}

		public void setShare_count(String share_count) {
			this.share_count = share_count;
		}

	}

	protected OGObject og_object;
	protected Share share;
	protected String id;

	public OGObject getOg_object() {
		return og_object;
	}

	public void setOg_object(OGObject og_object) {
		this.og_object = og_object;
	}

	public Share getShare() {
		return share;
	}

	public void setShare(Share share) {
		this.share = share;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
