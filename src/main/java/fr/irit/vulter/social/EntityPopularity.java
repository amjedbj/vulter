package fr.irit.vulter.social;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import fr.irit.vulter.bing.SearchResult;
import fr.irit.vulter.bing.WebSearch;
import fr.irit.vulter.facebook.GraphAPI;

public class EntityPopularity {

	private int cut = 3;

	protected static WebSearch websearch = new WebSearch();
	protected static GraphAPI graph = new GraphAPI();

	public EntityPopularity() {

	}

	public EntityPopularity(int cut) {
		this.cut = cut;
	}

	public double popularity(String entity) {
		List<SearchResult> results = websearch.search(entity, this.cut,
				true);
		int popularity = 0;
		for (SearchResult result : results) {
			try {
				URL url = new URL(result.getUrl());
				popularity = Math.max(popularity, graph.interactions(url));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		return popularity;
	}
}
