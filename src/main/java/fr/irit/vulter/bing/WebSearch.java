/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.bing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.List;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.ClientProtocolException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WebSearch {
	public static final String PROPERTIES = "bing.properties";
	protected String accountKeyEnc = "";

	public WebSearch() {
		String accountKey = this.getProperties().getProperty("bing.acount.key");
		byte[] accountKeyBytes = Base64
				.encodeBase64((accountKey + ":" + accountKey).getBytes());
		this.accountKeyEnc = new String(accountKeyBytes);
	}

	private String normalize(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		str = str.toLowerCase();
		str = str.replaceAll("\\W++", "-");
		return str;
	}

	private Properties getProperties() {

		try {
			Properties properties = new Properties();
			File file = new File(WebSearch.PROPERTIES);
			InputStream in = new FileInputStream(file);
			properties.load(in);
			return properties;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<SearchResult> search(String q, int count) {
		return this.search(q, count, false);
	}

	public List<SearchResult> search(String q, int count, boolean exact) {
		try {
			File cache = new File("cache/bing/" + this.normalize(q));
			String content = null;
			if (cache.exists()) {
				content = FileUtils.readFileToString(cache);
			} else {
				String qURL = "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Web";
				q = URLEncoder.encode(q, StandardCharsets.UTF_8.toString());
				if (exact) {
					q = "\"" + q + "\"";
				}
				qURL += "?Query=%27" + q + "%27";
				qURL += "&$top=" + count;
				qURL += "&$format=json";

				URL url = new URL(qURL);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				conn.setRequestProperty("Authorization", "Basic "
						+ this.accountKeyEnc);
				int statusCode = conn.getResponseCode();
				if (statusCode == 200) {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader((conn.getInputStream())));
					StringBuilder strBuilder = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						strBuilder.append(line);
					}
					content = strBuilder.toString();
					FileUtils.writeStringToFile(cache, content, "UTF-8");
				}
			}
			if (content != null) {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode results = mapper.readValue(content, JsonNode.class);
				List<SearchResult> objects = mapper.readValue(results.get("d")
						.get("results").toString(),
						new TypeReference<List<SearchResult>>() {
						});
				return objects;
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static void main(String[] args) throws ClientProtocolException,
			IOException {

		WebSearch bing = new WebSearch();
		String q = "Hot Wheels karambol kamion";
		bing.search(q, 10, true);
	}
}