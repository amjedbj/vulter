/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.lucene.search.spell.LevensteinDistance;
import org.apache.lucene.search.spell.StringDistance;

import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.TermImpl;

@SuppressWarnings("serial")
public class Dictionary extends TreeMap<String, Term> {

	public Dictionary() {

	}

	public void load(File input) throws IOException {
		CSVParser parser = CSVParser.parse(input, StandardCharsets.UTF_8,
				CSVFormat.TDF);
		for (CSVRecord record : parser) {
			Term term = new TermImpl();
			term.setText(record.get(0));
			term.setDf(Integer.parseInt(record.get(1)));
			term.setCf(Integer.parseInt(record.get(2)));
			this.put(term.getText(), term);
		}
	}

	public Term term(String term) {
		if (this.containsKey(term)) {
			return this.get(term);
		}
		return null;
	}

	public Collection<Term> fuzzy(String term) {
		Collection<Term> terms = new ArrayList<Term>();
		StringDistance distance = new LevensteinDistance();

		for (String str : this.keySet()) {
			double d = distance.getDistance(term, str);
			if (d > 0.75) {
				terms.add(this.get(str));
			}
		}
		return terms;
	}

}
