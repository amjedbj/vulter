/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;

import fr.irit.nestor.text.Textual;
import fr.irit.nestor.text.TextualImpl;

public class MultiFieldTextual extends TextualImpl {

	protected List<Textual> fields = new ArrayList<Textual>();

	private static String concat(List<String> fields) {
		StringBuilder text = new StringBuilder();

		for (String field : fields) {
			if (!text.toString().isEmpty())
				text.append(" ");
			text.append(field);
		}
		return text.toString();
	}

	public MultiFieldTextual(List<String> fields) {
		super(MultiFieldTextual.concat(fields));
		for (String field : fields) {
			this.fields.add(new TextualImpl(field));
		}
	}

	public MultiFieldTextual(List<String> fields, Analyzer analyzer) {
		super(MultiFieldTextual.concat(fields), analyzer);
		for (String field : fields) {
			field = (field != null) ? field : "";
			this.fields.add(new TextualImpl(field, analyzer));
		}
	}

	@Override
	public List<Textual> getFields() {
		return this.fields;
	}

}
