package fr.irit.vulter.scoring;

import java.util.Collections;

import fr.irit.nestor.searching.Query;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.searching.ScoreComparator;
import fr.irit.nestor.text.Textual;

public abstract class AbstractMultiFieldTextScorerImpl extends
		AbstractMultiFieldScorerImpl implements MultiFieldSimilarity {

	public AbstractMultiFieldTextScorerImpl() {
	}

	protected void lengthSwap(Textual o1, Textual o2) {
		if (o2.length() < o1.length()) {
			Textual t = o1;
			o1 = o2;
			o2 = t;
		}
	}

	@Override
	public void process(Query<?> query, ResultSet<?> resultSet) {
		for (ResultItem<?> resultItem : resultSet) {
			double score = this.getSimilarity(query,(MultiFieldTextual) resultItem.getTextual());
			resultItem.setScore(score);
		}
		Collections.sort(resultSet, new ScoreComparator());

	}

	@Override
	public int compare(Textual o1, MultiFieldTextual o2) {
		// TODO Implement a comparaison as separated multifieleds
		if (o1.length() < o1.length()) {
			return -1;
		}
		return 1;
	}

}
