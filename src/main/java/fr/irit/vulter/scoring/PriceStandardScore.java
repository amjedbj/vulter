/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.math3.stat.descriptive.UnivariateStatistic;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.util.MathArrays;

import fr.irit.nestor.scoring.AbstractScorerImpl;
import fr.irit.nestor.searching.Query;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.searching.ScoreComparator;
import fr.irit.vulter.ll4ir.api.ApiProduct;

/**
 * measure of the divergence of an individual experimental result from the most
 * probable result, the mean
 * 
 * @see http://en.wikipedia.org/wiki/Standard_score
 * @author Amjed Ben Jabeur
 *
 */
public class PriceStandardScore extends AbstractScorerImpl {

	public PriceStandardScore() {

	}

	protected double[] toArray(Collection<Double> values) {
		double[] res = new double[values.size()];
		int i = 0;
		for (Double value : values) {
			res[i] = value;
			i++;
		}
		return res;
	}

	@Override
	public void process(Query<?> query, ResultSet<?> resultSet) {
		Collection<Double> prices = new ArrayList<Double>();
		for (ResultItem<?> result : resultSet) {
			ApiProduct product = (ApiProduct) result.getDocument();
			prices.add(product.getContent().getPrice());
		}
		MathArrays.Function mean = new Mean();
		UnivariateStatistic standardScore = new StandardDeviation();
		double mu = mean.evaluate(this.toArray(prices));
		double sigma = standardScore.evaluate(this.toArray(prices));
		for (ResultItem<?> resultItem : resultSet) {
			ApiProduct product = (ApiProduct) resultItem.getDocument();
			double price = product.getContent().getPrice();
			double zscore = (price - mu) / sigma;
			double score = Math.abs(1d / zscore);
			resultItem.setScore(score);
		}
		Collections.sort(resultSet, new ScoreComparator());

	}

}
