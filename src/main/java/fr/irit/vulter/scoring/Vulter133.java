/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.Collections;

import fr.irit.nestor.searching.Query;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.searching.ScoreComparator;
import fr.irit.vulter.ll4ir.api.ApiProduct;
import fr.irit.vulter.social.EntityPopularity;

public class Vulter133 extends Vulter131 {

	protected double alpha = 0.25;

	public Vulter133() {
	}

	public String getName() {
		return "Vulter133-" + this.percentile;
	}

	private double logscale(int x) {
		return Math.log(1 + x);
	}

	@Override
	public void process(Query<?> query, ResultSet<?> resultSet) {
		super.process(query, resultSet);

		EntityPopularity epop = new EntityPopularity(3);
		int maxpop = 100;

		for (ResultItem<?> result : resultSet) {
			double popularity = 0d;
			double photos = 0d;
			try {
				ApiProduct product = (ApiProduct) result.getDocument();
				popularity = epop.popularity(product.getTitle());
				popularity = this.logscale((int) Math.min(popularity, maxpop))
						/ this.logscale(maxpop);

				photos = Math.min(product.getContent().getPhotos().size(), 10d) / 10d;
			} catch (Exception e) {
				e.printStackTrace();
			}
			int step = 2;
			double score = result.getScore() + step - step * (1 - popularity);
			score = score + step - step * (1 - photos);
			result.setScore(score);
		}
		Collections.sort(resultSet, new ScoreComparator());
	}

}
