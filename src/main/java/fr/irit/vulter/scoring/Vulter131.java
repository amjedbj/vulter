/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.Textual;

public class Vulter131 extends Vulter130 {

	public Vulter131() {
	}

	public String getName() {
		return "Vulter131-" + this.percentile;
	}

	@Override
	public double getSimilarity(Textual o1, MultiFieldTextual o2) {
		double score = super.getSimilarity(o1, o2);
		int qlength = o1.termSet().size();
		double qnorm = 0d;
		int i = 0;
		for (Textual field : o2.fields) {
			int terms = 0;
			for (Term term : o1.termSet()) {
				if (this.weights.get(i) > 0d) {
					terms += (field.freq(term.getText()) > 0) ? 1 : 0;
				}
			}
			double ratio = (double) terms / (double) qlength;
			qnorm = (ratio > qnorm) ? ratio : qnorm;
			i++;
		}
		score *= qnorm;
		return score;
	}

}
