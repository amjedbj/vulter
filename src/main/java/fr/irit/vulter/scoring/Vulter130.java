/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.commons.math3.util.MathArrays;

import fr.irit.nestor.scoring.IDF;
import fr.irit.nestor.searching.Query;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.searching.ScoreComparator;
import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiProduct;

public class Vulter130 extends AbstractMultiFieldTextScorerImpl {

	protected double percentile = 0.95;
	protected double k1 = 2d;
	protected long N = 5000;

	@SuppressWarnings("serial")
	protected List<Double> weights = new ArrayList<Double>() {
		{
			add(0d);
			add(0d);
			add(0d);
			add(0d);
			add(0d);
			add(35d);
			add(1d);
			add(0d);
			add(35d);
			add(0d);
			add(1d);
			add(0d);
			add(0d);
			add(0d);
			add(0d);
			add(38.4d);
			add(0d);
			add(1d);
		}
	};

	public Vulter130() {
	}

	public String getName() {
		return "Vulter130-" + this.percentile;
	}

	protected double[] toArray(Collection<Double> values) {
		double[] res = new double[values.size()];
		int i = 0;
		for (Double value : values) {
			res[i] = value;
			i++;
		}
		return res;
	}

	protected int getCategory(ApiProduct product) {
		return product.getContent().getCategory_id();
	}

	@Override
	public void process(Query<?> query, ResultSet<?> resultSet) {
		super.process(query, resultSet);
		MathArrays.Function percentile = new Percentile(this.percentile);
		Map<Integer, Collection<Double>> categoryScores = new TreeMap<Integer, Collection<Double>>();
		Map<Integer, Double> categoryPercentiles = new TreeMap<Integer, Double>();

		for (ResultItem<?> result : resultSet) {
			try {
				ApiProduct product = (ApiProduct) result.getDocument();
				int category = this.getCategory(product);
				if (!categoryScores.containsKey(category))
					categoryScores.put(category, new ArrayList<Double>());
				categoryScores.get(category).add(result.getScore());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (Integer key : categoryScores.keySet()) {
			categoryPercentiles.put(key,
					percentile.evaluate(this.toArray(categoryScores.get(key))));
		}

		for (ResultItem<?> resultItem : resultSet) {
			try {
				ApiProduct product = (ApiProduct) resultItem.getDocument();
				int category = product.getContent().getCategory_id();
				double categorySimilarity = 1+Math.log(1 + categoryScores.get(
						category).size())
						* categoryPercentiles.get(category);
				double score = resultItem.getScore() * categorySimilarity;
				resultItem.setScore(score);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Collections.sort(resultSet, new ScoreComparator());
	}

	@Override
	public double getSimilarity(Textual o1, MultiFieldTextual o2) {
		double score = 0.0d;
		for (Term term : o1.termSet()) {
			long df = term.getDf();
			double idf = IDF.OkapiBM25(df, this.N);
			double tf_ij = 0;
			int i = 0;
			for (Textual field : o2.fields) {
				if (this.weights.get(i) > 0d) {
					tf_ij += this.weights.get(i) * field.freq(term.getText());
				}
				i++;
			}
			double wij = (tf_ij / (this.k1 + tf_ij)) * idf;
			score += wij;
		}
		return score;
	}

}
