/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.Transformer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;

import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.Textual;
import fr.irit.vulter.ll4ir.api.ApiProduct;

public class ProductTransformer implements Transformer<ApiProduct, Textual> {

	protected Map<String, Term> dictionary = null;
	protected Analyzer analyzer = new HungarianAnalyzer();

	public ProductTransformer() {

	}

	public ProductTransformer(Map<String, Term> dictionary) {
		this.dictionary = dictionary;

	}

	public ProductTransformer(Analyzer analyzer) {
		this.analyzer = analyzer;

	}

	public ProductTransformer(Analyzer analyzer, Map<String, Term> dictionary) {
		this.analyzer = analyzer;
		this.dictionary = dictionary;

	}

	@Override
	public Textual transform(ApiProduct product) {

		List<String> fields = new ArrayList<String>();
		
		fields.add(Integer.toString(product.getContent().getAge_max())); // 0
		fields.add(Integer.toString(product.getContent().getAge_min())); // 1
		fields.add(product.getContent().getArrived()); // 2
		fields.add(Integer.toString(product.getContent().getAvailable())); // 3
		fields.add(Double.toString(product.getContent().getBonus_price())); // 4
		fields.add(product.getContent().getBrand()); // 5
		fields.add(product.getContent().getCategory()); // 6
		fields.add(Integer.toString(product.getContent().getCategory_id()));// 7
		fields.add((product.getContent().getCharacters() != null) ? product
				.getContent().getCharacters().toString() : "");  // 8
		fields.add(product.getContent().getDescription()); // 9
		fields.add(product.getContent().getMain_category()); // 10
		fields.add(Integer.toString(product.getContent().getMain_category_id())); // 11
		fields.add(Integer.toString(product.getContent().getGender())); // 12
		fields.add((product.getContent().getPhotos() != null) ? product
				.getContent().getPhotos().toString() : ""); // 13
		fields.add(Double.toString(product.getContent().getPrice())); // 14
		fields.add(product.getContent().getProduct_name()); // 15
		fields.add((product.getContent().getQueries() != null) ? product
				.getContent().getQueries().toString() : ""); // 16
		fields.add(product.getContent().getShort_description()); // 17

		Textual textual = new MultiFieldTextual(fields, this.analyzer);
		if (this.dictionary != null) {
			for (Term term : textual.termList()) {
				if (this.dictionary.containsKey(term.getText())) {
					Term entry = this.dictionary.get(term.getText());
					term.setDf(entry.getDf());
					term.setCf(entry.getCf());
				}
			}
			for (Textual field : textual.getFields()) {
				for (Term term : field.termList()) {
					if (this.dictionary.containsKey(term.getText())) {
						Term entry = this.dictionary.get(term.getText());
						term.setDf(entry.getDf());
						term.setCf(entry.getCf());
					}
				}
			}

		}
		return textual;
	}
}
