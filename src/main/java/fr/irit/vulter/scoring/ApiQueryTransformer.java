/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.Map;

import org.apache.commons.collections15.Transformer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.Textual;
import fr.irit.nestor.text.TextualImpl;
import fr.irit.vulter.ll4ir.api.ApiQuery;

public class ApiQueryTransformer implements Transformer<ApiQuery, Textual> {

	protected Map<String, Term> dictionary = null;
	protected Analyzer analyzer = new StandardAnalyzer();

	public ApiQueryTransformer() {

	}

	public ApiQueryTransformer(Analyzer analyzer) {
		this.analyzer = analyzer;

	}

	public ApiQueryTransformer(Map<String, Term> dictionary) {
		this.dictionary = dictionary;

	}

	public ApiQueryTransformer(Analyzer analyzer, Map<String, Term> dictionary) {
		this.analyzer = analyzer;
		this.dictionary = dictionary;

	}

	@Override
	public Textual transform(ApiQuery query) {
		Textual textual = new TextualImpl(query.getQstr(), this.analyzer);
		if (this.dictionary != null) {
			for (Term term : textual.termList()) {
				if (this.dictionary.containsKey(term.getText())) {
					Term entry = this.dictionary.get(term.getText());
					term.setDf(entry.getDf());
					term.setCf(entry.getCf());
				}
			}
		}
		return textual;
	}

}
