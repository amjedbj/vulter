package fr.irit.vulter.scoring;

import fr.irit.nestor.text.Textual;

public interface MultiFieldSimilarity {
	int compare(Textual o1, MultiFieldTextual o2);
	double getSimilarity(Textual o1, MultiFieldTextual o2);
}
