/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.vulter.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import fr.irit.nestor.scoring.FreqEasing;
import fr.irit.nestor.searching.Query;
import fr.irit.nestor.searching.ResultItem;
import fr.irit.nestor.searching.ResultSet;
import fr.irit.nestor.searching.ScoreComparator;
import fr.irit.vulter.ll4ir.api.ApiProduct;

public class CategoryScorePercentile extends FreqEasing {

	protected double percentile = 0.75;
	protected boolean category = true;
	protected boolean maincategory = true;

	public CategoryScorePercentile(double percentile, boolean category,
			boolean maincategory) {
		this.percentile = percentile;
		this.category = category;
		this.maincategory = maincategory;
	}

	public String getName() {
		return "CategoryScorePercentile-" + this.percentile + "-"
				+ ((this.category) ? 1 : 0) + ((this.maincategory) ? 1 : 0);
	};

	private double[] collectionToArray(Collection<Double> values) {
		double[] res = new double[values.size()];
		int i = 0;
		for (Double value : values) {
			res[i] = value;
			i++;
		}
		return res;
	}

	@Override
	public void process(Query<?> query, ResultSet<?> resultSet) {
		super.process(query, resultSet);

		Map<Integer, Collection<Double>> categories = new TreeMap<Integer, Collection<Double>>();
		Map<Integer, Collection<Double>> maincategories = new TreeMap<Integer, Collection<Double>>();

		for (ResultItem<?> result : resultSet) {
			try {
				ApiProduct product = (ApiProduct) result.getDocument();
				int category = product.getContent().getCategory_id();
				int maincategory = product.getContent().getMain_category_id();

				if (!categories.containsKey(category))
					categories.put(category, new ArrayList<Double>());
				if (!maincategories.containsKey(maincategory))
					maincategories.put(maincategory, new ArrayList<Double>());
				categories.get(category).add(result.getScore());
				maincategories.get(maincategory).add(result.getScore());

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Map<Integer, Double> pCategories = new TreeMap<Integer, Double>();
		Map<Integer, Double> pMaincategories = new TreeMap<Integer, Double>();

		Percentile percentile = new Percentile();
		for (Integer key : categories.keySet()) {
			pCategories.put(key, percentile.evaluate(
					this.collectionToArray(categories.get(key)),
					this.percentile));
		}
		for (Integer key : maincategories.keySet()) {
			pMaincategories.put(key, percentile.evaluate(
					this.collectionToArray(maincategories.get(key)),
					this.percentile));
		}

		for (ResultItem<?> result : resultSet) {
			try {
				ApiProduct product = (ApiProduct) result.getDocument();
				int category = product.getContent().getCategory_id();
				int maincategory = product.getContent().getMain_category_id();

				// double score = result.getScore();
				double pCategory = 1
						+ Math.log(categories.get(category).size())
						* pCategories.get(category) / 5;
				double pMaincategory = 1
						+ Math.log(maincategories.get(maincategory).size())
						* pMaincategories.get(maincategory) / 5;
				if (this.category && this.maincategory) {
					result.setScore(result.getScore() * pCategory
							* pMaincategory);
				} else if (this.category) {
					result.setScore(result.getScore() * pCategory);
				} else if (this.maincategory) {
					result.setScore(result.getScore() * pMaincategory);

				}
				/*
				 * System.out.println(product.getDocid() + "\t" +
				 * result.getScore() + "\t" + score + "\t" + pCategory + "(" +
				 * categories.get(category).size() + ")" + "\t" + pMaincategory
				 * + "(" + maincategories.get(maincategory).size() + ")" + "\t"
				 * + product.getContent().getProduct_name());
				 */
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Collections.sort(resultSet, new ScoreComparator());
	}

}
