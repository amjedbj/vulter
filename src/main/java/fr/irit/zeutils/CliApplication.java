/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.zeutils;


import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *  A generic Class for building console-based command-line applications in Java.
 * @author Lamjed Ben Jabeur <jabeur@irit.com>
 *
 */
public abstract class CliApplication {

	/**
	 * Command line options for the current application.
	 */
	protected Options options;
	
	/**
	 * Declares the command line options for the current application.
	 * @return The command line options.
	 */
	protected abstract Options getOptions();

	/**
	 * Parses the arguments array and extract the command line object.
	 * @param args The arguments array .
	 * @return The command line object
	 * @throws ParseException
	 */
	protected CommandLine parseLine(String[] args)
			throws ParseException {
		CommandLineParser parser = new BasicParser();
		return parser.parse(this.getOptions(), args);
	}

	/**
	 * Prints the help for the the current application.
	 */
	protected void printHelp() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(this.getClass().getName(), this.getOptions());
	}
	
	
}