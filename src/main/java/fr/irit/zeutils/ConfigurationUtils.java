/**
 * Copyright (C) 2015 IRIT (jabeur@irit.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.irit.zeutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationUtils {

	private static final String DEFAULT_CONFIGURATION = "config.properties";
	private static Properties properties;

	private static void init() {
		if (ConfigurationUtils.properties == null) {
			ConfigurationUtils.properties = ConfigurationUtils
					.getProperties(new File(
							ConfigurationUtils.DEFAULT_CONFIGURATION));
		}
	}

	public static Properties getProperties() {
		ConfigurationUtils.init();
		return ConfigurationUtils.properties;
	}

	public static Properties getProperties(File file) {
		Properties properties = new Properties();
		try {
			FileInputStream input = new FileInputStream(file);
			properties.load(input);
			input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}

	public static String getProperty(String key, String path) {
		Properties properties = ConfigurationUtils
				.getProperties(new File(path));
		return properties.getProperty(key);
	}
}