package fr.irit.nestor.text;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.KeywordAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

public class TextualImpl implements Textual {
	protected String text;
	protected Analyzer analyzer = null;
	protected List<Term> terms = null;
	protected Map<String, Integer> frequencies = null;

	public TextualImpl(String text) {
		this.setText(text);
	}

	public TextualImpl(String text, Analyzer analyzer) {
		this.analyzer = analyzer;
		this.setText(text);
	}

	protected void analyze() {
		try {
			if (this.analyzer == null) {
				this.analyzer = new StandardAnalyzer();
			}
			if (this.getText() != null) {
				this.terms = new ArrayList<Term>();
				this.frequencies = new TreeMap<String, Integer>();

				TokenStream tokenStream = this.analyzer.tokenStream(null,
						new StringReader(text));
				tokenStream.reset();
				OffsetAttribute offsetAttribute = tokenStream
						.addAttribute(OffsetAttribute.class);
				CharTermAttribute charTermAttribute = tokenStream
						.addAttribute(CharTermAttribute.class);
				KeywordAttribute keywordAttribute = tokenStream
						.addAttribute(KeywordAttribute.class);
				TypeAttribute typeAttribute = tokenStream
						.addAttribute(TypeAttribute.class);
				while (tokenStream.incrementToken()) {
					Term term = new TermImpl();

					term.setText(charTermAttribute.toString());
					term.setPosition(this.terms.size());
					term.setStartOffset(offsetAttribute.startOffset());
					term.setEndOffset(offsetAttribute.endOffset());
					term.setKeyword(keywordAttribute.isKeyword());
					term.setType(typeAttribute.type());
					term.setDf(1l);
					term.setCf(1l);
					term.setLength(term.getText().length());
					this.addToken(term);

				}
				tokenStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void addToken(Term term) {
		int freq = this.freq(term.getText());
		this.frequencies.put(term.getText(), freq + 1);
		this.terms.add(term);
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}

	public void setText(String text) {
		this.text = text;
		this.analyze();
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public List<Term> termList() {
		return this.terms;
	}

	@Override
	public Set<Term> termSet() {
		Set<Term> set = new TreeSet<Term>();
		set.addAll(this.terms);
		return set;
	}

	@Override
	public int freq(String term) {
		return (this.frequencies.containsKey(term)) ? this.frequencies
				.get(term) : 0;
	}

	@Override
	public String toString() {
		return this.getText();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextualImpl other = (TextualImpl) obj;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public int length() {
		return this.termList().size();
	}

	@Override
	public List<Textual> getFields() {
		List<Textual> fields = new ArrayList<Textual>();
		fields.add(this);
		return fields;
	}
}
