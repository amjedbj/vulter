package fr.irit.nestor.searching;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections15.Transformer;

import fr.irit.nestor.text.Term;
import fr.irit.nestor.text.Textual;

public abstract class FlexibleTextual<T> implements TextualTransformable<T> {

	protected T document;
	protected Transformer<T, Textual> transformer;

	public FlexibleTextual(T document, Transformer<T, Textual> transformer) {
		this.document = document;
		this.transformer = transformer;
	}

	@Override
	public T getDocument() {
		return this.document;
	}

	@Override
	public Textual getTextual() {
		return (this.document != null) ? this.transform(this.document) : null;
	}

	@Override
	public Textual transform(T arg0) {
		return this.transformer.transform(arg0);
	}

	@Override
	public String getText() {
		return (this.getTextual() != null) ? this.getTextual().getText() : null;
	}

	@Override
	public List<Term> termList() {
		return (this.getTextual() != null) ? this.getTextual().termList()
				: null;
	}

	@Override
	public Set<Term> termSet() {
		return (this.getTextual() != null) ? this.getTextual().termSet() : null;
	}

	@Override
	public int freq(String term) {
		return (this.getTextual() != null) ? this.getTextual().freq(term) : 0;
	}

	@Override
	public int length() {
		return (this.getTextual() != null) ? this.getTextual().length() : 0;
	}

	@Override
	public List<Textual> getFields() {
		return (this.getTextual() != null) ? this.getTextual().getFields()
				: null;
	}

}
