# Vulter

A ranking system for product search engine


## Getting Source Code

Vulter source code is available in a Git repository. Make sure git is installed on your system and clone the Vulter repository from Bitbucket. 

``` bash
git clone https://amjedbj@bitbucket.org/amjedbj/vulter.git
```

You can clone the sources to any directory on your system. If you have no preferences, choose `~/vulter` as that is the directory we will use below.

## Compiling project

Let's assume you cloned the Vulter repository like shown above to the directory `~/vulter`. Make sure Java 1.7 and Maven  are installed on your system. Execute the following commands to compile the project.

``` bash
cd ~/vulter
mvn install
```

## Running Vulter

When the compilation and installation finished successfully, you can execute on the available applications.


### List documents

The following command lists all documents from *REGIO Jatek* Web site available through the *Living Labs* API. To get the list of all document of particular Web site, you must provide a valid site' short name (i.e. *"R"*). Check the list of available Web sites on your *Living Labs* [dashboard](http://living-labs.net:5001/site/).

``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.ListDocuments -s R
```

This command is helpful to collect all documents from *Living Labs* API and store a local cache version.


### Generate term statistics

The following command enable generating term stats for a particular Web site available through the *Living Labs* API. You must provide a valid site' short name (i.e. *"R"*) and the path of the output  file (i.e. *"data/ll4ir/dictionary-hu.tsv"*).

``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.TermStats -s R -o data/ll4ir/stats/dictionary-hu.tsv
```

The generated Tab-separated values file `~/vulter/data/ll4ir/dictionary-hu.tsv`  list all terms in the collection with respective document frequency *df* and collection frequency *cf*. This is a sample output of term statistics application.

Term  | *df*  | *cf*
--- | --- | ---
angry | 66 | 113
baba | 300 | 329
... | ... | ...



### Test query

Test query application run a query and display results.  You must provide the query ID (i.e. *"R-q36"*) as well as a the scorer class (i.e. *"fr.irit.nestor.scoring.OkapiBM25"*).

``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.TestQuery -q R-q36 -f fr.irit.nestor.scoring.OkapiBM25
```

### Make a run

The MakeRun application enables to process queries and write results in [LL4IR run](http://doc.living-labs.net/en/latest/api-participant.html#run) format. The output file is formatted as a [ndjon] (http://ndjson.org/)  where each query run is written in new line.  You must provide a valid site' short name (i.e. *"R"*),  the run identifier  (i.e. *"myrun"*),  the scorer class (i.e. *"fr.irit.nestor.scoring.OkapiBM25"*) and the  path of the ndjson output  file (i.e. *"data/ll4ir/run.ndsjon"*).

``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.MakeRun -s R  -r myrun  -f fr.irit.nestor.scoring.OkapiBM25 -o data/ll4ir/runs/run.ndsjon
```

### Submit a run

The SubmitRun application enables to submit run to LL4IR.  You  the  path to the ndjson query runs  file (i.e. *"data/ll4ir/run.ndsjon"*).

``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.SubmitRun -i data/ll4ir/run.ndsjon
```

### View a run

The ViewRun application enables to process queries generate a visual output of results. The output file is formatted as a HTML file.  You must provide a valid site' short name (i.e. *"R"*), the scorer class (i.e. *"fr.irit.nestor.scoring.OkapiBM25"*) and the  path of the ndjson output  file (i.e. *"data/ll4ir/run.ndsjon"*).

``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.ViewRun
-s R  -f fr.irit.nestor.scoring.OkapiBM25 -o data/ll4ir/run.html
```

### Check a run

The CheckRun application enables to check submitted runs on LL4IR api. It counts the number of submitted queries by each runid.  You must provide a valid site' short name (i.e. *"R"*).
``` bash
java -cp target/vulter-1.0.0-SNAPSHOT-jar-with-dependencies.jar fr.irit.vulter.ll4ir.app.CheckRun
-s R
```

## Version

Vulter 1.0.0

## Citation
If you use this code, please refer to this paper:

> Lamjed Ben Jabeur, Laure Soulier and Lynda Tamine. 2015. IRIT at CLEF 2015: A product search model for head queries. In : Working Notes for CLEF 2015 Conference, CEUR Workshop Proceedings, septembre, 2015.

## Authors

The following people have contributed to this code:

* Lamjed Ben Jabeur [<jabeur@irit.fr>](mailto:jabeur@irit.fr)
* Laure Soulier [<soulier@irit.fr>](mailto:soulier@irit.fr)


## Copyright

Copyright © 2015 [IRIT](http://irit.fr/), University of Paul Sabatier, Toulouse, France

Licensed under the [Apache License](LICENSE.txt), Version 2.0: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)